*** Settings ***
Library  Selenium2Library
*** Variables ***
${search_word}  iPhone 7
*** Keywords ***
Verify Search Result
    Input Text  //input[@id='searchbox']    ${search_word}
    Press Keys  //input[@id='doSearch']  ENTER
    Sleep   3s
    Page Should Contain  ${search_word}

Choose An iPhone And Put To The Bucket
    Click Element  //*[@id="page-search"]/div[2]/div/div[1]/div/div[3]/div/ul/li[1]/div[2]/p/a
    Sleep   3s
    Mouse Down  //*[@id="page-product"]/div[3]/div/div[2]/div/div[1]/div[4]/div/div[2]/span[1]
    Click Element   //*[@id="page-product"]/div[3]/div/div[2]/div/div[1]/div[4]/div/div[2]/span[1]

Ordering And Registration
    Sleep  4s
    Mouse Over  //body/div[1]/div[1]/div[1]/div[3]/div[2]/form[1]/div[1]/div[1]/div[1]/select[1]
    Click Element  //body/div[1]/div[1]/div[1]/div[3]/div[2]/form[1]/div[1]/div[1]/div[1]/select[1]
    Click Element   //body/div[1]/div[1]/div[1]/div[3]/div[2]/form[1]/div[1]/div[1]/div[1]/select[1]/option[2]
    Click Element   //a[contains(text(),'Оформити замовлення')]
    Sleep   4s
    Click Element   //span[contains(text(),'Реєстрація')]
    Input Text  //body/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/form[1]/div[1]/div[1]/div[1]/input[1]  some2some@gmail.com
    Sleep   2s
    Click Element   //body/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/form[1]/div[1]/div[2]/input[1]

Clear Bucket
    Sleep   3s
    Go Back
    Go Back
    Sleep   5s
    Click Element   //body/div[1]/div[1]/div[1]/div[3]/div[2]/div[3]/i[1]
    Sleep   2s